'use strict';

let flock = require('flockos');
 
flock.appId = '866892d4-7fa8-4545-a16d-1606586c820c';
flock.appSecret = 'cae2b30e-8a41-438e-be84-649ed1e47af4';

let express = require('express');
let app = express();
app.use(flock.events.tokenVerifier);

app.post('/events', flock.events.listener);

flock.events.on('client.slashCommand', function (event, callback) {
	console.log('received slashCommand', arguments);

    // invoke the callback to send a response to the event 
    callback(null, { text: 'Received your command' });
});

app.listen(8000);

/*
flock.callMethod('chat.sendMessage', token, {
    to: 'u:wufu4udrcewerudu',
    text: 'hello'
}, function (error, response) {
    if (!error) {
        console.log(response);
    }
});*/