# flockoli

> Flockoli takes the fuss out of decision making around 'what's for lunch' using FlockOS.

## About

This project uses FlockOS.

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies
    
    ```
    cd path/to/devweek17-flock; npm install
    ```

3. (optional) if running locally, install ngrok and run

    ```
    ngrok http 80
    ```

4. Start your app
    
    ```
    npm start
    ```

## Help

